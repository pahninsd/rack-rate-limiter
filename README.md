# Rate Limiter Application

## Overview

This repository contains a Rate Limiter application that allows you to control and limit API requests based on API keys and tier configurations. The application is built using Sinatra and utilizes Redis to store API keys and tier mappings. The rate limiter runs on port 9292.

## Getting Started

Follow these steps to set up and test the Rate Limiter application:

1. **Build and Start Docker Containers:**
   ```bash
   docker-compose up --build
   ```

2. **Access Console:**
   ```bash
   docker-compose exec rate_limiter_app bash
   ```

3. **Open Ruby Console:**
   ```bash
   irb
   ```

4. **Add Test API Keys:**
   ```ruby
   require 'redis'
   cl = Redis.new(host: "redis")
   cl.set("api_key_tier:tier3:abc123", true)
   ```
   This adds 'abc123' to tier3. You can customize API keys and tiers as needed.

5. **Review and Change Configuration:**
   Review and modify the `app/src/config.yml` file if necessary.

6. **Run Unit Tests:**
   ```bash
   rspec
   ```

7. **Simulate API Requests:**
   ```bash
   for ((i=1; i<=60; i++)); do
     echo "counter: $i"
     echo "$(curl -s GET "localhost:9292/test" -H "Authorization: abc123" | jq)"
   done
   ```
   Adjust the API endpoint and key as required.

## Docker Compose Configuration

The provided `docker-compose.yml` file sets up the default Sinatra app running on port 8080 and connects to a Redis instance for storing API keys and tier mappings.

## Dynamic Configuration Updates

Changes made to API key data in Redis or modifications to the `app/src/config.yml` file take some time but are dynamically applied to the rate limiter without requiring a restart.

Feel free to replace the Sinatra app with any other upstream application to suit your needs.

## Note

Make sure to customize API keys, tiers, and configurations according to your application's requirements.

Happy rate limiting!
