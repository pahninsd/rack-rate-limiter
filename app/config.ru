require 'rack/proxy'
require_relative 'src/rate_limiter'
require 'redis'
require 'json'

# Rack application
class LimiterProxy
  def initialize
    @limiter = RateLimiter.new
    @proxy = Rack::Proxy.new
    @api_key_tiers = {}  # In-memory hash map
    @mutex = Mutex.new  # Mutex for thread safety
    @redis = Redis.new(host: 'redis')  # Redis client

    # Start a thread for periodic updates
    start_periodic_update_thread
  end

  def call(env)
    request = Rack::Request.new(env)
    auth_key = env['HTTP_AUTHORIZATION']
    tier_name = get_api_key_tier(auth_key)
    if auth_key.nil? && tier_name.nil?
      return [403, { 'Content-Type' => 'application/json' }, [{ message: 'Key not found' }.to_json]]
    end
    

    if @limiter.evaluate_request(auth_key, tier_name, request.request_method.downcase)
      # Rack::Proxy will handle the proxying of requests
      env['HTTP_X_FORWARDED_HOST'] = "upstream_app"
      env['HTTP_X_FORWARDED_PORT'] = "8080"
      @proxy.call(env)
    else
      [429, { 'Content-Type' => 'application/json' }, [{ message: 'Rate limit exceeded' }.to_json]]
    end
  end

  private

  def start_periodic_update_thread
    Thread.new do
      loop do
        update_api_key_tiers(fetch_new_tier_data_from_redis)
        sleep(900)
      end
    end
  end

  def fetch_new_tier_data_from_redis
    new_tier_data = {}

    # Iterate over each tier
    ['tier1', 'tier2', 'tier3'].each do |tier|
      # Fetch all keys for the current tier
      redis_keys = @redis.keys("api_key_tier:#{tier}:*")

      # Extract API key and tier information
      redis_keys.each do |key|
        api_key = key.split(':').last
        new_tier_data[api_key] = tier
      end
    end

    new_tier_data 
  end

  def update_api_key_tiers(new_tier_data)
    # Update the in-memory hash map with mutex for thread safety
    @mutex.synchronize do
      @api_key_tiers = new_tier_data
    end
  end

  def get_api_key_tier(api_key)
    # Your logic to retrieve the tier from the in-memory hash map with mutex
    @mutex.synchronize do
      @api_key_tiers[api_key]
    end
  end
end

run LimiterProxy.new
