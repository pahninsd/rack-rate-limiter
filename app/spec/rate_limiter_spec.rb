# src/spec/rate_limiter_spec.rb
require 'rspec'
require_relative '../src/rate_limiter'

RSpec.describe RateLimiter do
  let(:rate_limiter) { RateLimiter.new }
  let(:user) { 'test_user' }
  let(:tier_name) { 'test_tier' }
  let(:request_type) { 'GET' }

  describe '#evaluate_request' do
    it 'returns true if request count is within the threshold' do
      allow(rate_limiter).to receive(:fetch_threshold).and_return(10)
      allow(rate_limiter).to receive(:count_requests_in_time_window).and_return(5)

      result = rate_limiter.evaluate_request(user, tier_name, request_type)
      expect(result).to be_truthy
    end

    it 'returns false if request count exceeds the threshold' do
      allow(rate_limiter).to receive(:fetch_threshold).and_return(10)
      allow(rate_limiter).to receive(:count_requests_in_time_window).and_return(15)

      result = rate_limiter.evaluate_request(user, tier_name, request_type)
      expect(result).to be_falsey
    end
  end

  describe 'private methods' do
    before do
      allow(File).to receive(:exist?).and_return(true)
      allow(YAML).to receive(:load_file).and_return('limits' => { tier_name => { request_type.downcase => 5 } }, 'time_window' => 60, 'expiry_time' => 300)
    end

    describe '#fetch_threshold' do
      it 'reloads configuration if file is updated' do
        allow(File).to receive(:mtime).and_return(Time.now + 10)
        expect(rate_limiter).to receive(:reload!)

        rate_limiter.send(:fetch_threshold, tier_name, request_type)
      end

      it 'returns the correct threshold value' do
        expect(rate_limiter.send(:fetch_threshold, tier_name, request_type)).to eq(5)
      end
    end

    describe '#load_config' do
      it 'loads the configuration from the file' do
        expect(rate_limiter.send(:load_config)).to eq('limits' => { tier_name => { request_type.downcase => 5 } }, 'time_window' => 60, 'expiry_time' => 300)
      end
    end

    describe '#clean_up_old_data' do
      it 'removes old timestamps from user_time_series' do
        current_time = Time.now
        rate_limiter.instance_variable_get(:@user_time_series)[user] = [current_time - 400, current_time - 200]

        rate_limiter.send(:clean_up_old_data, user)
        expect(rate_limiter.instance_variable_get(:@user_time_series)[user]).to eq([current_time - 200])
      end
    end

    describe '#count_requests_in_time_window' do
      it 'returns the count of requests within the time window' do
        current_time = Time.now
        rate_limiter.instance_variable_get(:@user_time_series)[user] = [current_time - 70, current_time - 50, current_time - 30]

        count = rate_limiter.send(:count_requests_in_time_window, user)
        expect(count).to eq(2)
      end
    end

    describe '#update_time_series' do
      it 'adds the current timestamp to user_time_series' do
        current_time = Time.now
        rate_limiter.instance_variable_set(:@user_time_series, { user => []} )
        rate_limiter.send(:update_time_series, user)
        expect((rate_limiter.instance_variable_get(:@user_time_series)[user].last-current_time).to_i).to eq(0)
      end
    end
  end
end
