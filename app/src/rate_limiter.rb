require 'wongi-engine'
require 'yaml'

CONFIG_FILE_PATH = "/app/src/config.yml"

class RateLimiter
  def initialize
    load_engine
    @last_updated_at = nil
    @time_window = nil
    @expiry_time = nil
    @user_time_series = {}

    setup_rules
  end

  def evaluate_request(user, tier_name, request_type)
    threshold = fetch_threshold(tier_name, request_type)

    # expire old data
    clean_up_old_data(user)

    # Count the number of requests in the window
    request_count = count_requests_in_time_window(user)

    # Update the time series with the current request timestamp
    update_time_series(user)

    # Check if the request count exceeds the threshold
    request_count <= threshold
  end

  private

  def log(msg)
    logfile = File.open('/app/logs/app.log', 'a')
    logfile.puts(msg)
    logfile.close
  end

  def fetch_threshold(tier_name, request_type)
    reload! if File.mtime(CONFIG_FILE_PATH) > @last_updated_at

    @engine.entity(tier_name).get("#{request_type.downcase}_limit".to_sym)
  end

  def load_config
    if File.exist?(CONFIG_FILE_PATH)
      YAML.load_file(CONFIG_FILE_PATH)
    else
      {}
    end
  end

  def setup_rules
    load_config['limits'].each do |tier, actions|
      actions.each do |action, value|
        @engine << [tier, "#{action}_limit".to_sym, value]
      end
    end
    @time_window = load_config['time_window']
    @expiry_time = load_config['expiry_time']
    @last_updated_at = Time.now
  end

  def load_engine
    @engine = Wongi::Engine.create
  end

  def reset_engine
    @engine = nil
  end

  def reload!
    puts "Reloading.."
    reset_engine && load_engine && setup_rules
  end

  def clean_up_old_data(user)
    @user_time_series[user] ||= []
    @user_time_series[user].select! { |t| t > (Time.now - @expiry_time) }
  end

  def count_requests_in_time_window(user)
    @user_time_series[user].select { |t| t >= (Time.now - @time_window) }.size
  end

  def update_time_series(user)
    @user_time_series[user] << Time.now
  end
end
