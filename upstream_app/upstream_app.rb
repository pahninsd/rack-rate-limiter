require 'sinatra'

set :port, 8080
set :bind, '0.0.0.0'

before do
  content_type :json
end

get '/*' do
  { status: 'success', method: 'GET' }.to_json
end

post '/*' do
  { status: 'success', method: 'POST' }.to_json
end
